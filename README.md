# LCB Blog Application Development with Symfony 4

Well hi there! This repository holds the code
for the [LCB Application].

## Setup

If you've just downloaded the code, congratulations!!

To get it working, follow these steps:

**Download Composer dependencies**

Make sure you have [Composer installed](https://getcomposer.org/download/)
and then run:

```
composer install
```

You may alternatively need to run `php composer.phar install`, depending
on how you installed Composer.

**Clone projects in desired folder**

be sure to use php 7.2 or higher

`git clone git@bitbucket.org:MarkoRadivojevic/lcb_app.git`

` composer install`

Next step is to setup your .env file, after that you need to create database

` php bin/console doctrine:database:create `

After that we need to update database schema

` php bin/console doctrine:schema:update --force `

**Start the built-in web server**

You can use Nginx or Apache, but the built-in web server works
great:

```
php bin/console server:run
```

Now check out the site at `http://localhost:8000`

App demo version:
```
https://app.recursion.rs/
```
Have fun!