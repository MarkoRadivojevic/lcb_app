<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryFormType;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_USER")
 */
class CategoryController extends AbstractController
{
    /**
     * @Route("/category", name="article_category_list")
     */
    public function list(CategoryRepository $categoryRepo)
    {
        $categories = $categoryRepo->findAll();

        return $this->render('category/list.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/category/new", name="article_category_new")
     */
    public function new(EntityManagerInterface $em, Request $request)
    {
        $form = $this->createForm(CategoryFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Category $category */
            $category = $form->getData();

            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('article_category_list');
        }

        return $this->render('category/new.html.twig', [
            'categoryForm' => $form->createView()
        ]);
    }

    /**
     *@Route("/category/{id}/edit", name="article_category_edit")
     */
    public function edit(Category $category, Request $request, EntityManagerInterface $em)
    {
        $form = $this->createForm(CategoryFormType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('article_category_list', [
                'id' => $category->getId(),
            ]);
        }

        return $this->render('category/edit.html.twig', [
            'categoryForm' => $form->createView()
        ]);
    }

    /**
     *@Route("/category/{id}/delete", name="article_category_delete")
     */
    public function delete(Category $category, Request $request, EntityManagerInterface $em)
    {
        $currentCategoryContainsArticles = !$category->getArticles()->isEmpty();

        if ($currentCategoryContainsArticles) {
            $this->addFlash('danger', 'You cannot delete this Category! You have article that belongs to this category');
            return $this->redirectToRoute('article_category_list');
        }

        $em->remove($category);
        $em->flush();

        $this->addFlash('success', $category->getName() . ' category deleted!');

        return $this->redirectToRoute('article_category_list');
    }

}
