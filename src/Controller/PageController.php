<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\CommentFormType;
use App\Repository\ArticleRepository;
use App\Repository\CommentRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * @Route("/", name="homepage")
     * @Route("/{page}.html", name="staticPage")
     * @param string $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction($page="index", ArticleRepository $articleRepo)
    {
        $articles = $articleRepo->findAll();

        //TODO Add pagination

        try {
            return $this->render("page/{$page}.html.twig", [
                "page" => $page,
                'articles' => $articles,
                ]);
        } catch (\InvalidArgumentException $e) {
            $this->notFoundAction();
        }
        // Other exceptions are handled outside this controller, this is just that we get 404 instead of 500
        // when the template isn't found
    }

    /**
     * @Route("/blog/{slug}", name="article_show")
     */
    public function showArticle(Article $article, CommentRepository $commentRepo)
    {
        $form = $this->createForm(CommentFormType::class);

        $comments = $commentRepo->findBy([
            'article' => $article,
        ]);

        return $this->render('article/show.html.twig', [
            'article' => $article,
            'commentForm' => $form->createView(),
            'comments' => $comments,
        ]);
    }

    public function notFoundAction() {
        throw new NotFoundHttpException("Page not found");
    }

}
