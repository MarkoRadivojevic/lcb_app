<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleFormType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_USER")
 */
class ArticleController extends AbstractController
{
    /**
     * @Route("/article", name="article_list")
     */
    public function list(ArticleRepository $articleRepo)
    {
        $userId = $this->getUser()->getId();

        $articles = $articleRepo->findBy([
            'owner' => $userId,
        ]);

        //TODO Add pagination

        return $this->render('article/list.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/article/new", name="article_new")
     */
    public function new(EntityManagerInterface $em, Request $request)
    {
        $form = $this->createForm(ArticleFormType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Article $article */
            $article = $form->getData();
            $article->setOwner($this->getUser());

            $em->persist($article);
            $em->flush();

            $this->addFlash('success', 'New Article created!');

            return $this->redirectToRoute('article_list');
        }

        return $this->render('article/new.html.twig', [
            'articleForm' => $form->createView()
        ]);
    }

    /**
     *@Route("/article/{id}/edit", name="article_edit")
     */
    public function edit(Article $article, Request $request, EntityManagerInterface $em)
    {
        if (!$this->isUserOwnerOfArticle($article)) {
            throw new \Exception("You don't have permission to edit this post");
        }

        $form = $this->createForm(ArticleFormType::class, $article);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($article);
            $em->flush();

            $this->addFlash('success', 'Article Updated!');

            return $this->redirectToRoute('homepage');
        }

        return $this->render('article/edit.html.twig', [
            'articleForm' => $form->createView(),
        ]);
    }


    /**
     *@Route("/article/{id}/delete", name="article_delete")
     */
    public function delete(Article $article, Request $request, EntityManagerInterface $em)
    {
        if (!$this->isUserOwnerOfArticle($article)) {
            throw new \Exception("You don't have permission to delete this post");
        }

        $em->remove($article);
        $em->flush();

        $this->addFlash('success', $article->getTitle() . ' deleted!');

        return $this->redirectToRoute('article_list');
    }

    /**
     * @param Article $article
     * @return bool
     */
    public function isUserOwnerOfArticle(Article $article) {
        $userId = $this->getUser()->getId();
        $articleOwnerId = $article->getOwner()->getId();

        if ($userId !== $articleOwnerId) {
             return false;
        }

        return true;
    }

}
