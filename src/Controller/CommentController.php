<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use App\Form\CommentFormType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @IsGranted("ROLE_USER")
 */
class CommentController extends AbstractController
{
    /**
     * @Route("/comment", name="comment")
     */
    public function index()
    {
        return $this->render('comment/index.html.twig', [
            'controller_name' => 'CommentController',
        ]);
    }

    /**
     * @Route("/comment/new", name="article_comment_new")
     */
    public function new(EntityManagerInterface $em, Request $request, ArticleRepository $articleRepo)
    {
        $form = $this->createForm(CommentFormType::class);
        $form->handleRequest($request);

        //TODO Code bellow need to be refactored
        $articleSlug = basename($request->headers->get('referer'));
        $currentArticle = $articleRepo->findOneBy([
            'slug' => $articleSlug,
        ]);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Comment $comment */
            $comment = $form->getData();
            $comment->setAuthor($this->getUser());
            /** @var Article $currentArticle */
            $comment->setArticle($currentArticle);

            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('article_show', [
                'slug' => $articleSlug,
                'article' => $currentArticle
            ]);
        }

        return $this->render('article/show.html.twig', [
            'commentForm' => $form->createView()
        ]);
    }

    /**
     *@Route("/comment/{id}/delete", name="article_comment_delete")
     */
    public function delete(Comment $comment, Request $request, EntityManagerInterface $em)
    {
        if (!$this->isUserOwnerOfComment($comment)) {
            throw new \Exception("You don't have permission to delete this comment");
        }

        $em->remove($comment);
        $em->flush();

        $this->addFlash('success', 'Comment deleted!');

        return $this->redirectToRoute('article_show', [
            'slug' => $comment->getArticle()->getSlug()
        ]);
    }


    /**
     * @Route("/comment/{id}/edit", name="article_comment_edit")
     */
    public function edit(Comment $comment, EntityManagerInterface $em, Request $request, ArticleRepository $articleRepo, $id)
    {
        $form = $this->createForm(CommentFormType::class, $comment);
        $form->handleRequest($request);

        //TODO Code bellow need to be refactored
        $articleSlug = basename($request->headers->get('referer'));
        $currentArticle = $articleRepo->findOneBy([
            'slug' => $articleSlug,
        ]);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($comment);
            $em->flush();

            return $this->redirectToRoute('article_show', [
                'slug' => $comment->getArticle()->getSlug()
            ]);
        }

        return $this->render('comment/edit.html.twig', [
            'commentForm' => $form->createView(),
        ]);
    }

    /**
     * @param Comment $comment
     * @return bool
     */
    public function isUserOwnerOfComment(Comment $comment) {
        $userId = $this->getUser()->getId();
        $commentOwnerId = $comment->getAuthor()->getId();

        if ($userId !== $commentOwnerId) {
            return false;
        }

        return true;
    }

    //TODO ADD edit comments logic

}
